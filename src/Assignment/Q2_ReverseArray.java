package Assignment;

public class Q2_ReverseArray {

	    public static void main(String[] args){
	    	int[] a= {11,19,15,10};
			System.out.println("Original Array : ");
			
			for(int i=0;i<a.length;i++) {
				System.out.println(a[i] +" ");
			}
			System.out.println("Arrays in reverse order : ");
			for(int i=a.length-1;i>=0;i--) {
				System.out.println(a[i] +" ");
	      
			}
	    }
}
